<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quotation;
use Illuminate\Support\Facades\DB;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Telegram;
use App\Models\TelegramData;

class TelegramController extends Controller
{
    public function webhook(Request $request)
    {
        $json = $request->getContent();

        $bot_api_key  = '1393697549:AAH-3hsjmCnzZOAyJPtBe-bHMNw7Y6vEzDU';
        $bot_username = 'jungle_courses_bot';

        try {
            $telegram = new Telegram($bot_api_key, $bot_username);

        $this->doAction($request);

        } catch (TelegramException $e) {
             echo $e->getMessage();
        }


        return "true";
    }

    private function getChatId($request) {
        if (isset($request->callback_query["message"]["chat"]["id"])) {
            return $request->callback_query["message"]["chat"]["id"];
        }
        return $request->message["chat"]["id"];
    }

    private function getMessageId($request) {
        return $request->callback_query["message"]["message_id"];
    }

    private function getPayloadData($request) {
        if (isset($request->callback_query["data"])) {
            return $request->callback_query["data"];
        }
        return "/start";
    }

    private function doAction($request) {
        $payloadData = $this->getPayloadData($request);
        $messageId = $this->getMessageId($request);

        if ($payloadData == "/start" || $payloadData == "main") {
            if (!isset($messageId)) {
                $this->sendActions($request, TelegramData::$base);
            } else {
                $this->updateActions($request, $messageId, TelegramData::$base, "Підтримка");
            }
        } else if (isset(TelegramData::$nested[$payloadData])) {
            $this->updateActions($request, $messageId, TelegramData::$nested[$payloadData], "Підтримка");
        } else if(isset(TelegramData::$text[$payloadData])) {
            $this->sendMessage($request, TelegramData::$text[$payloadData]);
            sleep(1);
            $this->sendActions($request, TelegramData::$nested[TelegramData::getParent($payloadData)]);
        } else {
            $this->sendActions($request, TelegramData::$base);
        }
    }

    private function sendActions($request, $data) {
        \Longman\TelegramBot\Request::sendMessage([
            'chat_id' => $this->getChatId($request),
            'text'  => "Підтримка",
            'reply_markup' => array(
                'inline_keyboard' => $data,
                'one_time_keyboard' => true,
                'resize_keyboard' => true
            )
        ]);
    }

    private function updateActions($request, $messageId, $data, $title) {
        \Longman\TelegramBot\Request::editMessageText([
            'chat_id' => $this->getChatId($request),
            'message_id' => $messageId,
            'text'  => $title,
            'reply_markup' => array(
                'inline_keyboard' => $data,
                'one_time_keyboard' => true,
                'resize_keyboard' => true
            )
        ]);
    }

    private function sendMessage($request, $text) {
        \Longman\TelegramBot\Request::sendMessage([
            'chat_id' => $this->getChatId($request),
            'text'  => $text,
            'parse_mode' => "markdown"
        ]);
    }

    private function utf8_urldecode($str) {
        $str = preg_replace("/u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($str));
        return html_entity_decode($str,null,'UTF-8');;
    }
}
